import {
    LoginForm,
    ProFormText,
} from '@ant-design/pro-components';
import { useMemo, useState } from 'react';
import className from 'classnames';
import logo from '@/static/logo.jpg';
import style from './index.less';
import { v4 as uuidv4 } from 'uuid';

interface LoginData {
    principal: string;
    credentials: string;
    imageCode: string;
    sessionUUID?: string;
}

const Login = () => {
    const [codeLoading, setCodeLoading] = useState(true);
    const [uuid, setUuid] = useState(() => uuidv4());
    const Imgurl = useMemo(() =>
        `https://bjwz.bwie.com/mall4w/captcha.jpg?uuid=${uuid}`,
        [uuid]
    )
    const handleImg = async () => {
        setCodeLoading(true);
        setUuid(uuidv4());
    }
    const getFinish = async (values: LoginData) => {
        console.log(values);
    }
    return (
        <div className={style.loginWrapper}>
            <LoginForm
                logo={logo}
                title="八维电商系统"
                subTitle="仅限1912A 10组内部使用"
                onFinish={getFinish}
            >
                <ProFormText placeholder={'账号'} name='principal'></ProFormText>
                <ProFormText.Password placeholder={'密码'} name='credentials'></ ProFormText.Password>
                <div className={style.codewrap}>
                    <span className={style.codeInp}><ProFormText placeholder={'验证码'} name='imageCode'></ProFormText></span>
                    <span className={className({
                        [style.loading]: codeLoading
                    }, style.codeImage)}>
                        <img
                            onClick={handleImg}
                            src={Imgurl}
                            onError={() => {
                                setCodeLoading(false)
                            }}
                            onLoad={() => {
                                setCodeLoading(false)
                            }}
                            alt="" />
                    </span>
                </div>
            </LoginForm>
        </div>
    )
}

export default Login;

